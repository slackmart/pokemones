"""
This module configures the pokemon django app.
"""

from django.apps import AppConfig


class PokemonConfig(AppConfig):
    """Config class holds pokemon app configuration"""
    name = 'pokemon'
