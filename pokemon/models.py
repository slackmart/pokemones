"""
Pokemon django app models
"""

from django.db import models


class Pokemon(models.Model):
    """Identifies a pokemon"""
    name = models.CharField(max_length=100)
    is_default = models.BooleanField(default=False)
    base_experience = models.IntegerField(null=True)
    height = models.IntegerField(null=True)
    order = models.IntegerField(null=True)
    weight = models.IntegerField(null=True)

    def __str__(self):
        return f'{self.id} => {self.name}'


class Ability(models.Model):
    """Pokemon's ability model"""
    name = models.CharField(max_length=200)
    is_hidden = models.BooleanField(default=False)

    pokemon = models.ForeignKey(Pokemon,
                                on_delete=models.CASCADE,
                                related_name='abilities')

    def __str__(self):
        return f'{self.pokemon} >> {self.name}'
