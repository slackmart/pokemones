"""
Viewsets for the pokemon api
"""

from rest_framework import viewsets

from .models import Ability, Pokemon
from .serializers import (
    AbilitySerializer, PokemonReadSerializer, PokemonSerializer
)
from .api_helper import PokemonApi


class PokemonViewSet(viewsets.ModelViewSet):
    """API endpoint that allows pokemons to be listed or edited"""
    queryset = Pokemon.objects.all()

    def get_serializer_class(self):
        """Overrides get_serializer_class in order to swap the serializer
        class"""
        if self.request.method in ['GET']:
            return PokemonReadSerializer
        return PokemonSerializer

    def get_queryset(self):
        """get_queryset is being overriden here in order to allow filtering
        by name.

        Auto backup pockemon and its abilities if it comes from pokeapi.
        """
        queryset = super().get_queryset()
        name = self.request.query_params.get('name')
        new_pokemon = None
        if name:
            name = name.lower()
            queryset = queryset.filter(name=name)
            if queryset.count() == 0:  # Pokemon needs to be fetched from pokeapi
                new_pokemon, _ = PokemonApi.fetch_and_save(name)
                return Pokemon.objects.filter(id=new_pokemon.id)

        return queryset


class AbilityViewSet(viewsets.ModelViewSet):
    """API endpoint that allows abilities to be listed or edited"""
    queryset = Ability.objects.all()
    serializer_class = AbilitySerializer
