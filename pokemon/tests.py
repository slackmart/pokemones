"""
Tests for the pokemon django app goes here.
"""
from unittest.mock import patch

from django.test import TestCase
from pokepy.resources_v2 import PokemonResource

from .models import Ability, Pokemon


class IndexTestCase(TestCase):
    """Test index homepage"""


class PokemonListTestCase(TestCase):
    """Pokemons are fetched from pokeapi and stored in local database"""

    def setUp(self):
        """Configures test cases"""
        self.one = {'pokemon': Pokemon.objects.create(name='kakuna')}
        self.one['ability'] = Ability.objects.create(
            name='shaked', pokemon=self.one['pokemon'])

        self.second = {'pokemon': Pokemon.objects.create(name='Pikachu')}
        self.second['ability'] = Ability.objects.create(
            name='electric', pokemon=self.second['pokemon'])

        self.third = {'pokemon': Pokemon.objects.create(name='Snorlax')}
        self.third['ability'] = [
            Ability.objects.create(
                name='electric', pokemon=self.third['pokemon']),
            Ability.objects.create(
                name='shaked', pokemon=self.third['pokemon'])
        ]
        self.expected = [
            {
                'name': self.one['pokemon'].name,
                'abilities': [str(self.one['ability'])]
            }, {
                'name': self.second['pokemon'].name,
                'abilities': [str(self.second['ability'])]
            }, {
                'name': self.third['pokemon'].name,
                'abilities': [str(a) for a in self.third['ability']]
            }
        ]

    def test_get_all(self):
        """Fetch all pokemons from database"""
        resp = self.client.get('/api/pokemones', follow=True)
        assert resp.status_code == 200, resp.status_code
        assert resp.json() == self.expected, f'{resp.json()} != {self.expected}'

    def test_get_single(self):
        """Fetch single pokemon from database. Verify it contains pokemon's
        abilities. Pokemon id starts at 1"""
        pokemon_id = 2
        expected = self.expected[pokemon_id - 1]

        resp = self.client.get(f'/api/pokemones/{pokemon_id}', follow=True)
        assert resp.status_code == 200, resp.status_code

        json_response = resp.json()
        assert json_response == expected, json_response
        assert self.one['ability'].name not in json_response['abilities'][0], json_response

    def test_pokemon_has_multiple_abilities(self):
        """Fetch single pokemon from database. Verify it contains multiple
        pokemon's abilities"""
        pokemon_id = 3
        expected = self.expected[pokemon_id - 1]

        resp = self.client.get(f'/api/pokemones/{pokemon_id}', follow=True)
        assert resp.status_code == 200, resp.status_code

        json_response = resp.json()
        assert json_response == expected, f'{expected} != {json_response}'
        for ability in self.third['ability']:
            assert str(ability) in json_response['abilities'], f'{ability}, {json_response["abilities"]}'

    def test_get_single_pokemon_by_name(self):
        """Get pokemon by query string name"""
        pokemon_id = 1
        expected = self.expected[pokemon_id - 1]

        resp = self.client.get(f'/api/pokemones/?name={expected["name"]}', follow=True)
        assert resp.status_code == 200, resp.status_code

        json_response = resp.json()
        assert json_response == [expected], f'[{expected}] != {json_response}'

    @patch('pokemon.api_helper.PokemonApi.fetch')
    def test_fallback_to_pokemon_api(self, mock_api):
        """Fetch from pokeapi if pokemon does not exist in local
        database"""
        name = 'Charmander'
        expected = {
            'name': name.lower(),
            'abilities': [
                '4 => charmander >> solar-power', '4 => charmander >> blaze'
            ]
        }
        abilities = [
            {
                'ability': {'name': 'solar-power'}
            },
            {
                'ability': {'name': 'blaze'},
            }
        ]

        mock_api.return_value = [
            PokemonResource(id=4, name=name.lower(), abilities=abilities)
        ]
        resp = self.client.get(f'/api/pokemones/?name={name}', follow=True)

        assert resp.status_code == 200, resp.status_code
        mock_api.assert_called_once_with(name.lower())
        json_response = resp.json()
        assert json_response == [expected], f'{json_response} != [{expected}]'
