"""
Business logic goes here, it includes:
- consuminng third part APIs,
- Backup objects
"""

from pokepy import V2Client
from .models import Pokemon
from .serializers import AbilitySerializer, PokemonSerializer


class PokemonApi:
    """Isolates Pokemon API business logic into a convenient class"""
    cli = V2Client()

    @staticmethod
    def fetch(name):
        """Redirects and returns data from V2Client get_pokemon method"""
        return PokemonApi.cli.get_pokemon(name)

    @staticmethod
    def fetch_and_save(name, save_abilities=True):
        """Fetch and trigger backup methods for ability and pokemon models"""
        try:
            pokemon_response = PokemonApi.fetch(name)
            return PokemonApi.save(pokemon_response[0], save_abilities)
        except Exception as e:
            print(f'Pokemon {name} was not found!')

        return Pokemon.objects.filter(name=name), []

    @staticmethod
    def save(pokemon_resource, save_abilities):
        """Uses the rest framework serializers in order to validate and backup
        objects into local database

        returns: a tuple containing the resources just saved
        """
        data = {'name': pokemon_resource.name}

        pokemon_serializer = PokemonSerializer(data=data)
        if pokemon_serializer.is_valid():
            pokemon = pokemon_serializer.save()

        if save_abilities:
            abilities = [
                {
                    'name': a.ability.name,
                    'pokemon': pokemon.id
                } for a in pokemon_resource.abilities
            ]
            abilities = PokemonApi.save_abilities(abilities)

        return pokemon, abilities

    @staticmethod
    def save_abilities(abilities):
        """Loop over abilities, validate and save objects"""
        abilities_objects = []
        for ability_payload in abilities:
            ability_serializer = AbilitySerializer(data=ability_payload)
            if ability_serializer.is_valid():
                abilities_objects.append(ability_serializer.save())

        return abilities_objects
