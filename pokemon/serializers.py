"""
Serializers for the pokemon app viewsets
"""

from rest_framework import serializers
from .models import Ability, Pokemon


class AbilitySerializer(serializers.ModelSerializer):
    """Serializer for the ability model"""
    class Meta:
        model = Ability
        fields = ('name', 'pokemon')


class PokemonSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer for the pokemon model, it allows us to link the pokemon
    model and define how it'll serialized"""
    class Meta:
        model = Pokemon
        fields = ('name',)


class PokemonReadSerializer(PokemonSerializer):
    """Serializer for the pokemon model, it allows us to link the pokemon
    model and define how it'll serialized"""
    abilities = serializers.StringRelatedField(many=True, read_only=True)

    class Meta(PokemonSerializer.Meta):
        fields = PokemonSerializer.Meta.fields + ('abilities',)
