"""pokemones URL Configuration"""

from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter
from pokemon import viewsets

router = DefaultRouter()
router.register(r'pokemones', viewsets.PokemonViewSet)
router.register(r'abilities', viewsets.AbilityViewSet)

urlpatterns = [
    path('', TemplateView.as_view(template_name="pokemon/index.html")),
    path('api/', include(router.urls))
]
