Pokemones
=========

Given a Pokemon’s name, returns that Pokemon’s abilities.

* Uses an external service for the API
* Cache external API responses (persistent cache)

Getting started
---------------

::

   $ python3 -m venv pokevenv
   $ . pokevenv/bin/activate
   (pokevenv) $ pip install -r requirements.txt
   (pokevenv) $ ./manage.py migrate
   (pokevenv) $ ./manage.py runserver

Running the tests and get coverage report
-----------------------------------------

::

   (pokevenv) $ coverage run --source='.' manage.py test
   (pokevenv) $ coverage report
   (pokevenv) $ coverage html
   $ firefox htmlcov/index.html

Visit http://localhost:8000 and http://localhost:8000/api
